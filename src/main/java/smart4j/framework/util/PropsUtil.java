package smart4j.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author XuYunXuan
 * @ClassName: PropsUtil
 * @Description:
 * @date 2017-09-18 19:36
 */
public final class PropsUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropsUtil.class);

    /**
     * 加载属性文件
     */
    public static Properties loadProps(String fileName){
        Properties props = null;
        InputStream is = null;
        try {
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            if(is == null){
                throw new IOException(fileName+"文件不存在");
            }
            props = new Properties();
            props.load(is);
        }catch (IOException e){
            LOGGER.error("加载配置文件失败",e);
        }finally {
            if(is != null){
                try {
                    is.close();
                } catch (IOException e) {
                    LOGGER.error("关闭输入流失败",e);
                }
            }
            return props;
        }
    }

    /**
     * 获取字符型属性
     */
    public static String getString(Properties props,String key){
        return getString(props,key,"");
    }
    /**
     * 获取字符型属性,可指定默认值
     */
    public static String getString(Properties props, String key, String defaultValue) {
        String value = defaultValue;
        if(props.containsKey(key)){
            value = props.getProperty(key);
        }
        return value;
    }

    /**
     *
     * 获取int值
     */
    public static int getInt(Properties props,String key){
        return getInt(props,key,0);
    }
    /**
     *
     * 获取int值,指定默认值
     */
    public static int getInt(Properties props, String key, int defaultValue) {
        int value = defaultValue;
        if(props.containsKey(key)){
            value = CastUtil.castInt(props.getProperty(key));
        }
        return value;
    }


    /**
     *
     * 获取boolean值
     */
    public static boolean getBoolean(Properties props,String key){
        return getBoolean(props,key,false);
    }
    /**
     *
     * 获取boolean值,指定默认值
     */
    public static boolean getBoolean(Properties props, String key, Boolean defaultValue) {
        boolean value = defaultValue;
        if(props.containsKey(key)){
            value = CastUtil.castBoolean(props.getProperty(key));
        }
        return value;
    }

}
