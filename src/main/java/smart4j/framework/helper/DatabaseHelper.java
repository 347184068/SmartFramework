package smart4j.framework.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author XuYunXuan
 * @ClassName: DatabaseHelper
 * @Description:
 * @date 2017-09-23 18:39
 */
public final class DatabaseHelper {

    private static Logger LOGGER = LoggerFactory.getLogger(DatabaseHelper.class);

    private static final String driver = ConfigHelper.getJdbcDriver();

    private static final String url = ConfigHelper.getJdbcUrl();

    private static final String username = ConfigHelper.getJdbcUsername();

    private static final String password = ConfigHelper.getJdbcPassword();

    private static ThreadLocal<Connection> connContainer = new ThreadLocal<Connection>();

    public static Connection getConnection(){
        Connection conn = connContainer.get();
        try {
            if(conn == null){
                Class.forName(driver);
                conn = DriverManager.getConnection(url,username,password);
                if (conn != null) {
                    connContainer.set(conn);
                }
            }
        }catch (Exception e){
            LOGGER.error("创建数据库连接失败",e);
            throw new RuntimeException(e);
        }
        return conn;
    }

    public static Connection closeConnection(){
        Connection conn = connContainer.get();
        try {
            if(conn != null){
                conn.close();
            }
        }catch (Exception e){
            LOGGER.error("数据库连接关闭失败",e);
            throw new RuntimeException(e);
        }finally {
            connContainer.remove();
        }
        return conn;
    }

    /**
     * 开启事务
     */
    public static void beginTransaction() {
        Connection conn = getConnection();
        if (conn != null) {
            try {
                conn.setAutoCommit(false);
            } catch (SQLException e) {
                LOGGER.error("开启事务出错！", e);
                throw new RuntimeException(e);
            } finally {
                connContainer.set(conn);
            }
        }
    }

    /**
     * 提交事务
     */
    public static void commitTransaction() {
        Connection conn = getConnection();
        if (conn != null) {
            try {
                conn.commit();
                conn.close();
            } catch (SQLException e) {
                LOGGER.error("提交事务出错！", e);
                throw new RuntimeException(e);
            } finally {
                connContainer.remove();
            }
        }
    }

    /**
     * 回滚事务
     */
    public static void rollbackTransaction() {
        Connection conn = getConnection();
        if (conn != null) {
            try {
                conn.rollback();
                conn.close();
            } catch (SQLException e) {
                LOGGER.error("回滚事务出错！", e);
                throw new RuntimeException(e);
            } finally {
                connContainer.remove();
            }
        }
    }


}
