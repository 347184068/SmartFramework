package smart4j.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * @author XuYunXuan
 * @ClassName: CodecUtil
 * @Description:
 * @date 2017-09-21 20:00
 */
public class CodecUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(CodecUtil.class);

    /**
     * url编码
     */
    public static String encodeURL(String source){
        String target;
        try {
            target = URLEncoder.encode(source,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("编码失败",e);
            throw new RuntimeException(e);
        }
        return target;
    }

    /**
     * url解码
     */
    public static String decodeURL(String source){
        String target;
        try {
            target = URLDecoder.decode(source,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("解码失败",e);
            throw new RuntimeException(e);
        }
        return target;
    }
}
