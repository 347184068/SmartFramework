package smart4j.framework.proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author XuYunXuan
 * @ClassName: ProxyManager
 * @Description:
 * @date 2017-09-22 20:25
 */
public class ProxyManager {

    @SuppressWarnings("unchecked")
    public static <T> T createProxy(final  Class<?> targetClass, final List<Proxy> proxyList){
        return (T) Enhancer.create(targetClass, new MethodInterceptor() {
            public Object intercept(Object targetObject, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                return new ProxyChain(targetClass,targetObject,method,methodProxy,objects,proxyList).doProxyChain();
            }
        });
    }
}
