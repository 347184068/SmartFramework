package smart4j.framework.bean;

/**
 * @author XuYunXuan
 * @ClassName: FormParam
 * @Description:
 * @date 2017-09-24 19:09
 */
public class FormParam {

    private String fieldName;
    private Object fieldValue;

    public FormParam(String fieldName, Object fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Object getFieldValue() {
        return fieldValue;
    }
}
