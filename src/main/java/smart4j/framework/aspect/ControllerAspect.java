package smart4j.framework.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smart4j.framework.proxy.AspectProxy;
import smart4j.framework.annotation.Aspect;
import smart4j.framework.annotation.Controller;

import java.lang.reflect.Method;

/**
 * @author XuYunXuan
 * @ClassName: ControllerAspect
 * @Description:
 * @date 2017-09-22 20:36
 */
@Aspect(Controller.class)
public class ControllerAspect extends AspectProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerAspect.class);

    private long begin;


    @Override
    public void before(Class<?> cls, Method method, Object[] params) {
        LOGGER.debug("---------begin--------");
        LOGGER.debug(String.format("class: %s",cls.getName() ));
        LOGGER.debug(String.format("method: %s", method.getName()));
        begin = System.currentTimeMillis();
    }

    @Override
    public void after(Class<?> cls, Method method, Object[] params) {
        LOGGER.debug(String.format("time: %dms", System.currentTimeMillis() - begin));
        LOGGER.debug("---------end--------");
    }
}
