package smart4j.framework.bean;

/**
 * @author XuYunXuan
 * @ClassName: 返回数据对象
 * @Description:
 * @date 2017-09-21 19:37
 */
public class Data {

    private Object model;

    public Data(Object model){
        this.model = model;
    }

    public Object getModel() {
        return model;
    }
}
