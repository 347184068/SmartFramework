package smart4j.framework.util;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author XuYunXuan
 * @ClassName: ArrayUtil
 * @Description:
 * @date 2017-09-20 20:12
 */
public final class ArrayUtil {

    public static boolean isEmpty(Object[] array){
        return ArrayUtils.isEmpty(array);
    }


    public static boolean isNotEmpty(Object[] array){
        return !ArrayUtils.isEmpty(array);
    }


}
