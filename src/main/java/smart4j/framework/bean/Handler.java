package smart4j.framework.bean;

import java.lang.reflect.Method;

/**
 * @author XuYunXuan
 * @ClassName: 封装Action信息
 * @Description:
 * @date 2017-09-20 20:24
 */
public class Handler {

    private Class<?> controllerClass;

    private Method actionMethod;

    public Handler(Class<?> controllerClass,Method actionMethod){
        this.controllerClass = controllerClass;
        this.actionMethod = actionMethod;
    }

    public Class<?> getControllerClass() {
        return controllerClass;
    }

    public Method getActionMethod() {
        return actionMethod;
    }
}
