package smart4j.framework.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author XuYunXuan
 * @ClassName: FileUtil
 * @Description:
 * @date 2017-09-25 20:18
 */
public final class FileUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    public static String getRealFileName(String fileName){
        return FilenameUtils.getName(fileName);
    }

    public static File createFile(String filePath){
        File file;
        try {
            file = new File(filePath);
            File parentFile = file.getParentFile();
            if(!parentFile.exists()){
                FileUtils.forceMkdir(parentFile);
            }
        }catch (Exception e){
            LOGGER.error("创建文件失败",e);
            throw new RuntimeException(e);
        }
        return file;
    }

}
