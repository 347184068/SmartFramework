package smart4j.framework;

import smart4j.framework.helper.*;
import smart4j.framework.util.ClassUtil;

/**
 * @author XuYunXuan
 * @ClassName: HelperLoader
 * @Description:
 * @date 2017-09-20 20:44
 */
public final class HelperLoader {

    public static void init(){
        Class<?>[] classeList = {
                ClassHelper.class,
                BeanHelper.class,
                AopHelper.class,
                IocHelper.class,
                ControllerHelper.class
        };
        for (Class<?> cls : classeList) {
            ClassUtil.loadClass(cls.getName());
        }
    }

}
