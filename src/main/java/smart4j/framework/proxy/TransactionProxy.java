package smart4j.framework.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smart4j.framework.annotation.Transaction;
import smart4j.framework.bean.Data;
import smart4j.framework.helper.DatabaseHelper;

import java.awt.image.DataBuffer;
import java.lang.reflect.Method;

/**
 * @author XuYunXuan
 * @ClassName: TransactionProxy
 * @Description:
 * @date 2017-09-23 18:58
 */
public class TransactionProxy implements Proxy {

    public static final Logger LOGGER = LoggerFactory.getLogger(TransactionProxy.class);

    private static final ThreadLocal<Boolean> FLAG_HOLDER = new ThreadLocal<Boolean>(){
        @Override
        protected Boolean initialValue() {
            return false;
        }
    };

    public Object doProxy(ProxyChain proxyChain) throws Throwable {
        Object result;
        boolean flag = FLAG_HOLDER.get();
        Method method = proxyChain.getTargetMethod();
        if(!flag && method.isAnnotationPresent(Transaction.class)){
            FLAG_HOLDER.set(true);
            try {
                DatabaseHelper.beginTransaction();
                LOGGER.debug("开启事务");
                result = proxyChain.doProxyChain();
                DatabaseHelper.commitTransaction();
                LOGGER.debug("提交事务");
            }catch (Exception e){
                DatabaseHelper.rollbackTransaction();
                LOGGER.debug("回滚事务");
                throw e;
            }finally {
                FLAG_HOLDER.remove();
            }
        }else {
            result = proxyChain.doProxyChain();
        }
        return result;
    }
}
